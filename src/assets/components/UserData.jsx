import React from "react";

const UserData = ({ name, address, hobby }) => {
  return (
    <div className="w-full flex justify-between px-3 py-2 border shadow-md shadow-black">
      <div className="flex flex-col w-1/2 items-start gap-3">
        <span className="text-3xl">{name}</span>
        <span className="text-lg">{address}</span>
      </div>
      <div className="flex w-1/2 justify-center items-center gap-3">
        <span className="text-3xl text-green-400">{hobby}</span>
      </div>
    </div>
  );
};

export default UserData;
