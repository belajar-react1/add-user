import {
  AppBar,
  Button,
  Container,
  Stack,
  Toolbar,
  Typography,
} from "@mui/material";
import React from "react";
import AdbIcon from "@mui/icons-material/Adb";

const Navbar = ({ onOpen }) => {
  return (
    <AppBar position="static" className="px-5 py-3">
      <Stack
        direction={"row"}
        alignItems={"center"}
        justifyContent={"space-between"}
        className="w-full"
      >
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <AdbIcon sx={{ display: { xs: "none", md: "flex" }, mr: 1 }} />
            <Typography
              variant="h6"
              noWrap
              component={"a"}
              href="/"
              sx={{
                mr: 2,
                display: { xs: "none", md: "flex" },
                fontFamily: "monospace",
                fontWeight: 700,
                letterSpacing: ".3rem",
                color: "inherit",
                textDecoration: "none",
              }}
            >
              MY APP
            </Typography>
          </Toolbar>
        </Container>
        <Stack direction={"row"} justifyContent={"left"} marginRight={"3rem"}>
          <Button
            onClick={onOpen}
            variant="contained"
            color="warning"
            sx={{ width: "8rem" }}
          >
            Add User
          </Button>
        </Stack>
      </Stack>
    </AppBar>
  );
};

export default Navbar;
