import { useState } from 'react';
import Navbar from './assets/components/Navbar';
import { Stack, TextField } from '@mui/material';
import UserData from './assets/components/UserData';
import UserDialog from './assets/components/UserDialog';

function App() {

  const [users, setUsers] = useState([]);
  const [open, setOpen] = useState(false);

  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [hobby, setHobby] = useState("");

  const handleOpen = () => {
    setOpen(true);
  }

  const handleClose = () => {
    setOpen(false);
  }

  const clearField = () => {
    setName("");
    setAddress("");
    setHobby("");
  }

  const handleSubmit = () => {
    setUsers([...users, { name, address, hobby }]);
    clearField();
    handleClose();
  }

  return (
    <>
      <Navbar onOpen={handleOpen} />
      <UserDialog
        isOpen={open}
        onClose={handleClose}
        onSubmit={handleSubmit}
      >
        <TextField label={"Nama"} value={name} onChange={e => setName(e.target.value)} className='m-2 w-52' />
        <TextField label={"Address"} value={address} onChange={e => setAddress(e.target.value)} className='m-2 w-52' />
        <TextField label={"Hobby"} value={hobby} onChange={e => setHobby(e.target.value)} className='m-2 w-52' />
      </UserDialog>
      <Stack
        className={`w-full`}
        gap={3}
      >
        {users.map((user, index) => (
          <UserData key={index} name={user.name} address={user.address} hobby={user.hobby} />
        ))}
      </Stack>
    </>
  );
}

export default App;
